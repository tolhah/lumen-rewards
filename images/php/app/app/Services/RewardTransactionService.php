<?php

namespace App\Services;

use App\Models\RewardTransaction;
use App\Services\RewardService;
use App\Services\UserService;
use Carbon\Carbon;

class RewardTransactionService{

	public function __construct()
    {
        $this->rewardService = new RewardService;
        $this->userService = new UserService;
    }

    public function saveRewardTransaction($request){

        // Get today date
        $today = Carbon::now();
        // Get daily limit reward information
        $reward = $this->rewardService->findByDay($today->toDateString());
        // Did user already get reward?
        $isUserRewarded = $this->userService->hasBeenRewarded($request->user_id, $today->toDateString());
        if($isUserRewarded)
            return response('{"message": "Pengguna sudah mendapatkan reward"}', 404)
                    ->header('Content-Type', 'application/json');
        // Get random reward amount
        $rewardAmount = $this->userService->generateRandomReward($request->user_id);
        if($rewardAmount == 0)
            return response('{"message": "Pengguna tidak tersedia"}', 404)
                    ->header('Content-Type', 'application/json');
        // Compare daily limit reward with reward amount
    	if($reward->limit >= $rewardAmount){
            // Substract remaining limit & save
            $reward->limit = $reward->limit - $rewardAmount;
            $reward->save();
            // Save reward transaction
            $request->request->add(['amount' => $rewardAmount]);
            $rewardTransaction = RewardTransaction::create($request->all());

            return response()->json($rewardTransaction, 201);
        }else{
            return response('{"message": "Limit tidak tersedia"}', 404)
                    ->header('Content-Type', 'application/json');
        }
    }
}