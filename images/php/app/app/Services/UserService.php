<?php

namespace App\Services;

use App\Models\User;
use App\Models\RewardTransaction;

class UserService{
	
	public function hasBeenRewarded($user_id, $today)
	{
		$isUserRewarded = RewardTransaction::where('user_id', '=', $user_id)
                                            ->where('created_at', 'like', $today . '%')
                                            ->count();
        if($isUserRewarded > 0)
        	return true;
        else
        	return false;
	} 

	public function generateRandomReward($user_id)
	{
		$user = User::find($user_id);
		if($user){
			$range = range($user->min_reward, $user->max_reward, 1000);
			$rewardNumber = $range[mt_rand(0, count($range)-1)];
			return $rewardNumber;
		}else
			return 0;
	}

}