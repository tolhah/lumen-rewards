<?php

namespace App\Services;

use App\Models\Reward;

class RewardService{

	// Get daily limit reward information
	public function findByDay($day){
		return Reward::where('day', '=', $day)->firstOrFail();
	}

	// Post new reward limit
	public function save(){
		return Reward::save();
	}
	
}