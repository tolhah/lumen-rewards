<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('api/users',  ['uses' => 'UserController@showAllUsers']);
$app->get('api/users/{id}', ['uses' => 'UserController@showOneUser']);
$app->get('api/users/{id}/reward', ['uses' => 'UserController@showRandomReward']);
$app->post('api/users', ['uses' => 'UserController@create']);
$app->delete('api/users/{id}', ['uses' => 'UserController@delete']);
$app->put('api/users/{id}', ['uses' => 'UserController@update']);

$app->post('api/reward-transactions',  ['uses' => 'RewardTransactionController@create']);

$app->get('api/reward/today-limit',  ['uses' => 'RewardController@showTodayLimit']);
$app->post('api/reward',  ['uses' => 'RewardController@create']);