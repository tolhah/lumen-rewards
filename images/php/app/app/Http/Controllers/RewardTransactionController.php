<?php

namespace App\Http\Controllers;

use App\Services\RewardTransactionService;
use Illuminate\Http\Request;

class RewardTransactionController extends Controller
{

    public function __construct()
    {
        $this->rewardTransactionService = new RewardTransactionService;
    }

    public function create(Request $request)
    {        
        return $this->rewardTransactionService->saveRewardTransaction($request);
    }
}