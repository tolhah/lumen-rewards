<?php

namespace App\Http\Controllers;

use App\Models\Reward;
use App\Services\RewardService;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RewardController extends Controller
{
    public function __construct()
    {
        $this->rewardService = new RewardService;
    }

    public function showTodayLimit()
    {
        $today = Carbon::now();
        return $this->rewardService->findByDay($today->toDateString());;
    }

    public function create(Request $request)
    {
        $reward = Reward::create($request->all());

        return response()->json($reward, 201);
    }
}