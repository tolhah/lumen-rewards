# Docker + Lumen with Nginx and MySQL

This setup is great for writing quick apps in PHP using Lumen from an any Docker client. It uses docker-compose to setup the application services.

## Clone this repo

```bash
git clone https://github.com/idtolhah/lumen-rewards.git
cd lumen-rewards
```

## Create Lumen App

now, go into the `images\php` and see directory named `app`
that is the REST API app project folder
```bash
cd images/php
```

### Configuration

To change configuration values, look in the `docker-compose.yml` file and change the `php` container's environment variables. These directly correlate to the Lumen environment variables.

## Docker Setup

### [Docker for Mac](https://docs.docker.com/docker-for-mac/)

### [Docker for Windows](https://docs.docker.com/docker-for-windows/)

### [Docker for Linux](https://docs.docker.com/engine/installation/linux/)

### Build & Run

```bash
docker-compose up --build -d
```

Navigate to [http://localhost:80](http://localhost:80) and you should see something like this
![image](Lumen_browser.png)

Success! You can now start developing your Lumen app on your host machine and you should see your changes on refresh! Classic PHP development cycle. A good place to start is `images/php/app/routes/web.php`.

Feel free to configure the default port 80 in `docker-compose.yml` to whatever you like.

### Stop Everything

```bash
docker-compose down
```



## THE REST API APP

### Show Daily Limit Reward
![image](daily_reward_limit.PNG)

### Show existing users
![image](users.PNG)

### User with id 1 is trying to take reward
![image](reward-transactions-1.PNG)

### User with id 1 is trying to take reward but has been rewarded in a same day
![image](reward-transactions-1-f.PNG)

### User with id 2 is trying to take reward
![image](reward-transactions-2.PNG)

### User with id 2 is trying to take reward but has been rewarded in a same day
![image](reward-transactions-2-f.PNG)

### We can see Daily Limit Reward decresed
![image](limit_semakin_berkurang.PNG)

### Until the limit is unavailable
![image](Limit_tidak_tersedia.PNG)